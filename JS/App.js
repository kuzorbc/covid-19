var City = document.getElementById("City");
var Positive = document.getElementById("pos");
var Negative = document.getElementById("neg");
var Recovered = document.getElementById("rec");

var proslo = null;
var ovo;
function kosova() {
    City.innerHTML = "Kosova";
    Positive.innerHTML = "467";
    Negative.innerHTML = "2447";
    Recovered.innerHTML = "214";
    ovo = document.getElementById("Kosovo");
    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function mitrovica(ovo) {
    City.innerHTML = "Mitrovica";
    Positive.innerHTML = "20";
    Negative.innerHTML = "100";
    Recovered.innerHTML = "40";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}


function novoberda(ovo) {
    City.innerHTML = "Novobërda";
    Positive.innerHTML = "15";
    Negative.innerHTML = "76";
    Recovered.innerHTML = "10";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}
function leposavic(ovo) {
    City.innerHTML = "Leposavic";
    Positive.innerHTML = "50";
    Negative.innerHTML = "243";
    Recovered.innerHTML = "24";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function podujeva(ovo) {

    City.innerHTML = "Podujeva";
    Positive.innerHTML = "25";
    Negative.innerHTML = "354";
    Recovered.innerHTML = "18";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}
function zvecan(ovo) {
    City.innerHTML = "Zveçan";
    Positive.innerHTML = "5";
    Negative.innerHTML = "35";
    Recovered.innerHTML = "10";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}
function zubinPotok(ovo) {
    City.innerHTML = "Zubin Potok";
    Positive.innerHTML = "24";
    Negative.innerHTML = "58";
    Recovered.innerHTML = "15";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}
function istog(ovo) {
    City.innerHTML = "Istog";
    Positive.innerHTML = "5";
    Negative.innerHTML = "45";
    Recovered.innerHTML = "12";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}
function skenderaj(ovo) {
    City.innerHTML = "Skenderaj";
    Positive.innerHTML = "1";
    Negative.innerHTML = "34";
    Recovered.innerHTML = "5";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function vushtrri(ovo) {
    City.innerHTML = "Vushtrri";
    Positive.innerHTML = "8";
    Negative.innerHTML = "26";
    Recovered.innerHTML = "6";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function obiliq(ovo) {
    City.innerHTML = "Obiliq";
    Positive.innerHTML = "1";
    Negative.innerHTML = "14";
    Recovered.innerHTML = "2";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function prishtin(ovo) {
    City.innerHTML = "Prishtinë";
    Positive.innerHTML = "188";
    Negative.innerHTML = "498";
    Recovered.innerHTML = "35";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function kamenica(ovo) {
    City.innerHTML = "Kamenica";
    Positive.innerHTML = "8";
    Negative.innerHTML = "78";
    Recovered.innerHTML = "13";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function peja(ovo) {
    City.innerHTML = "Pejë";
    Positive.innerHTML = "42";
    Negative.innerHTML = "267";
    Recovered.innerHTML = "24";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function klina(ovo) {
    City.innerHTML = "Klinë";
    Positive.innerHTML = "3";
    Negative.innerHTML = "159";
    Recovered.innerHTML = "16";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function malishev(ovo) {
    City.innerHTML = "Malishevë";
    Positive.innerHTML = "32";
    Negative.innerHTML = "142";
    Recovered.innerHTML = "10";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function gllogovc(ovo) {
    City.innerHTML = "Gllogovc";
    Positive.innerHTML = "32";
    Negative.innerHTML = "158";
    Recovered.innerHTML = "7";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function fushKosov(ovo) {
    City.innerHTML = "Fushë Kosovë";
    Positive.innerHTML = "16";
    Negative.innerHTML = "89";
    Recovered.innerHTML = "23";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function lipjan(ovo) {
    City.innerHTML = "Lipjanë";
    Positive.innerHTML = "10";
    Negative.innerHTML = "125";
    Recovered.innerHTML = "18";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function decan(ovo) {
    City.innerHTML = "Deçanë";
    Positive.innerHTML = "3";
    Negative.innerHTML = "122";
    Recovered.innerHTML = "4";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function gjakova(ovo) {
    City.innerHTML = "Gjakova";
    Positive.innerHTML = "21";
    Negative.innerHTML = "243";
    Recovered.innerHTML = "16";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function rahovec(ovo) {
    City.innerHTML = "Rahovec";
    Positive.innerHTML = "4";
    Negative.innerHTML = "109";
    Recovered.innerHTML = "1";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function gjilan(ovo) {
    City.innerHTML = "Gjilan";
    Positive.innerHTML = "16";
    Negative.innerHTML = "347";
    Recovered.innerHTML = "6";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function viti(ovo) {
    City.innerHTML = "Viti";
    Positive.innerHTML = "8";
    Negative.innerHTML = "230";
    Recovered.innerHTML = "2";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function kacanik(ovo) {
    City.innerHTML = "Kaçanik";
    Positive.innerHTML = "8";
    Negative.innerHTML = "129";
    Recovered.innerHTML = "6";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function shterpca(ovo) {
    City.innerHTML = "Shtërpca";
    Positive.innerHTML = "5";
    Negative.innerHTML = "233";
    Recovered.innerHTML = "10";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function prizren(ovo) {
    City.innerHTML = "Prizren";
    Positive.innerHTML = "24";
    Negative.innerHTML = "344";
    Recovered.innerHTML = "2";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function dragash(ovo) {
    City.innerHTML = "Dragash";
    Positive.innerHTML = "2";
    Negative.innerHTML = "108";
    Recovered.innerHTML = "1";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function ferizaj(ovo) {
    City.innerHTML = "Ferizaj";
    Positive.innerHTML = "29";
    Negative.innerHTML = "186";
    Recovered.innerHTML = "4";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function suharek(ovo) {
    City.innerHTML = "Suharekë";
    Positive.innerHTML = "13";
    Negative.innerHTML = "103";
    Recovered.innerHTML = "2";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}

function shtime(ovo) {
    City.innerHTML = "Shtime";
    Positive.innerHTML = "6";
    Negative.innerHTML = "126";
    Recovered.innerHTML = "3";

    if (proslo == null) { }
    else {
        proslo.classList.toggle("active");
    }

    proslo = ovo;
    proslo.classList.toggle("active");
}